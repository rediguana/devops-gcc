"use strict";

const request = require("supertest");
const assert = require("assert");
const delegate = require("../delegates/auth");
const app = require("../app");
const http = require("http");
const expect = require("chai").expect;

const onError = error => {
  if (error.syscall !== "listen") {
    throw error;
  }

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      process.exit(1);
      break;
    case "EADDRINUSE":
      process.exit(1);
      break;
    default:
      throw error;
  }
};

describe("auth testing", () => {
  const server = http.createServer(app.callback());
  server.listen(6000);
  server.on("error", onError);

  describe("setup", done => {
    it("Deberia devolver 400 si se llama sin nombre o password", done => {
      request(server)
        .post("/")
        .expect(400)
        .end(done);
    });
  });

  describe("Con correo inválido", () => {
    it("Deberia devolver 401", done => {
      request(server)
        .post("/")
        .send({
          email: "a@gmail.com",
          password: "gcc123"
        })
        .expect(401)
        .end(done);
    });
  });

  describe("Con contraseña inválida", () => {
    it("Deberia devolver 401", done => {
      request(server)
        .post("/")
        .send({
          email: "prueba@gcc.pol.una.py",
          password: "asafdg"
        })
        .expect(401)
        .end(done);
    });
  });

  describe("Con credenciales válidas", () => {
    it("Deberia devolver 200", done => {
      request(server)
        .post("/")
        .send({
          email: "prueba@gcc.pol.una.py",
          password: "gcc123"
        })
        .expect(200)
        .end(done);
    });
  });

  after(() => server.close());
});
