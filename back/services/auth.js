const delegate = require("../delegates/auth");
const koaBody = require("koa-body");
const Boom = require("boom");
const cors = require("koa2-cors");

async function auth(ctx) {
  const { email, password } = ctx.request.body;
  const authorized = await delegate.authorize({ user: email, password });
  if (authorized === true) {
    ctx.response.status = 200;
    ctx.response.body = { data: "Usuario logueado" };
  } else {
    return authorized;
  }
}

exports.register = router => {
  router.post(
    `/`,
    koaBody(),
    async (ctx, next) => {
      try {
        await next();
      } catch (error) {
        // Si el error no es un Boom error, hacemos default a un bad implementation
        if (!Boom.isBoom(error)) {
          error = Boom.badImplementation();
        }
        // Tratamiento de errores Boom
        ctx.status = error.output.statusCode;
        if (error.data) error.output.data = error.data;
        ctx.body = error.output;
      }
    },
    cors({
      credentials: true,
      allowMethods: ["GET", "POST", "DELETE", "PUT", "OPTIONS"],
      allowHeaders: ["Content-Type", "Authorization", "Accept"]
    }),
    auth
  );
};
