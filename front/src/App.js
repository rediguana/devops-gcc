import React, { useState, useEffect } from "react";
import SignIn from "./SignIn";
import axios from "axios";
import {
  Snackbar,
  Button,
  IconButton,
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
  CardActions
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
function App() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [open, setOpen] = React.useState(false);
  const [message, setMessage] = useState("");
  const [isLoggedIn, setLogin] = useState(false);

  useEffect(() => {
    if (localStorage.getItem("loggedIn")) {
      console.log(localStorage.getItem("loggedIn"));
      setLogin(localStorage.getItem("loggedIn"));
    }
  }, []);

  function handleClose(event, reason) {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  }

  const backendRequest = () =>
    axios
      .post("https://staging-back-gcc.herokuapp.com", { email, password })
      .then(response => {
        if (response.status === 200) {
          setLogin(true);
          localStorage.setItem("loggedIn", true);
          return;
        }
        console.log(response.payload.message);
        setMessage(response.payload.message);
        localStorage.setItem("loggedIn", false);
        setLogin(false);
      })
      .catch(error => {
        console.log(JSON.stringify(error));
        setMessage(error.response.data.payload.message);
        setOpen(true);
      });

  return (
    <div className="App" style={{ height: "100%" }}>
      {!isLoggedIn && (
        <SignIn
          setEmail={setEmail}
          setPassword={setPassword}
          login={backendRequest}
        />
      )}
      {console.log(isLoggedIn)}

      {isLoggedIn && (
        <Card style={{ width: 500, margin: "auto", marginTop: "auto" }}>
          <CardActionArea>
            <CardMedia
              image="https://cdn-images-1.medium.com/max/2600/1*EBXc9eJ1YRFLtkNI_djaAw.png"
              title="Contemplative Reptile"
              style={{ height: 250 }}
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                DevOps
              </Typography>
              <Typography component="p">
                Metodología de trabajo basada en el desarrollo de código que usa
                nuevas herramientas y prácticas para reducir la tradicional
                distancia el área de sistemas y operaciones.
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button
              size="small"
              color="primary"
              onClick={() => {
                localStorage.setItem("loggedIn", false);
                setLogin(false);
              }}
              style={{ marginLeft: "auto" }}
            >
              LOGOUT
            </Button>
          </CardActions>
        </Card>
      )}
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left"
        }}
        open={open}
        autoHideDuration={2000}
        onClose={handleClose}
        ContentProps={{
          "aria-describedby": "message-id"
        }}
        message={<span id="message-id">{message}</span>}
        action={[
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            onClick={handleClose}
          >
            <CloseIcon />
          </IconButton>
        ]}
      />
    </div>
  );
}

export default App;
