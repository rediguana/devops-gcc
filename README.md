# devops-gcc

Devops project for the course Gestion de Centros de Cómputos - Facultad Politécnica 2019. 
Login with unitary testing to try Gitlab CI/CD and Heroku testing.

Se agregaron 4 pasos o stages a seguir de acuerdo al ambiente en que se esté realizando.
    - ver: verificación de requerimientos básicos
    - init: inicialización y descarga de paquetes 
    - test: donde se ejecutan los test del front end y del back end
    - deploy: donde se deploya a la instancia de heroku (Solo corresponde a los ambientes test y master).
    
Estos stages se siguen secuencialmente.
El ambiente de desarrollo solo ejecuta hasta los test, ya que no se debe deployar nada.
El stage de deploy es particular a cada ambiente (test y master), en el caso de test se deploya a dos instancias de heroku en staging para el 
front end y el back end. Lo mismo se hace para el caso de master pero en las instancias de producción.

Se configuraron tres 3 ambientes de desarrollo 
    - develop: donde se realiza el desarrollo de la página
    - test: homologación de los desarrollos
    - master: Código en producción